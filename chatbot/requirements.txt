#tensorflow==2.8.0
numpy==1.22.2
keras==2.8.0
nltk==3.7
Flask==2.0.3
Flask-SocketIO==5.1.1
