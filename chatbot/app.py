from flask import Flask, render_template
from flask_socketio import SocketIO, emit, send
import test

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret'
socketio = SocketIO(app)

@app.route('/')
def index():
    return render_template('index.html')

@socketio.on('connect')
def test_connect(auth):
    send('selamat datang, saya adalah chatbot, coba tanyakan apa saja mungkin saya bisa jawab...')
    print('Client connected')

@socketio.on('disconnect')
def test_disconnect():
    print('Client disconnected')

@socketio.on('message')
def handle_message(message):
    print('message: ' + str(message))
    response = test.chatbot_response(message)
    send(response)


if __name__ == '__main__':
    socketio.run(app, debug = True, host='0.0.0.0')

