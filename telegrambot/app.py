import telebot
import test

# bot name: ProsaChatBot
bot = telebot.TeleBot('5207676109:AAHkW_2XGgbhvmYy0aza1RcBjcmneUGSbwo')

# Handle '/start' and '/help'
@bot.message_handler(commands=['help', 'start'])
def send_welcome(message):
    bot.reply_to(message, 'selamat datang, saya adalah chatbot, coba tanyakan apa saja mungkin saya bisa jawab...')

# Handle all other messages with content_type 'text' (content_types defaults to ['text'])
@bot.message_handler(func=lambda message: True)
def bot_message(message):
    #print(message)
    if message.content_type == 'text':
        response = test.chatbot_response(message.text)
    else:
        response = 'coba kirimkan pesan teks'
    bot.reply_to(message, response)

print('telebot is running')
bot.infinity_polling()
